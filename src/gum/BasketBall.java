package gum;


import gum.Ball;
import gum.BasketballBucket;

// применяем наследие
public class BasketBall extends Ball {

	private final BasketballBucket basketballBucket;

	public BasketBall(double radius, BasketballBucket basketballBucket) {
		// вызываем конструктор родителя
		super(radius, basketballBucket);
		this.basketballBucket = basketballBucket;
	}


	public void throwBallToBasket(){
		use();
	}

	@Override
	public int getBallSize() {
		if (radius < 10.5) {
			return 5;
		} else {
			return 7;
		}
	}

	@Override
	public String getName() {
		return "basketball";
	}

	// реализуем доступ к инапсулированному полю через асессор(гетер)
	public BasketballBucket getBasketballBucket() {
		return basketballBucket;
	}
}
