package gum;

import java.util.Random;

public class BasketballBucket extends SportEquipment {

	@Override
	public void use() {
		System.out.println("Защитник стоит под кольцом");
	}

	// применяем полиморфизм
	@Override
	public boolean handle(Ball ball) {
		Random random = new Random();

		if (random.nextInt(101) > 50) {
			System.out.println("Мяч заблокирован");

			return true;
		} else {
			System.out.println("Мяч забит");

			return false;
		}
	}

	@Override
	public String toString() {
		return "BasketballBucket";
	}
}
