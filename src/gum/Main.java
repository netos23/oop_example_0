package gum;

import gum.BasketballGum;
import gum.Gum;
import gum.SoccerGum;
import gum.Equipment;

import java.util.Scanner;

public class Main {

	/*
	* Структура проекта следующая:
	* 	есть спортзал и оборудование, оборудование двух видов мячи и инвентарь
	* 	в спортзале можно запросить оборудование или поиграть
	*
	* */

	private static String welcomeMessage = "Во что хотите сыграть?\n" +
			"1 - баскетбол\n" +
			"2 - футбол\n";

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println(welcomeMessage);

		int chose = in.nextInt();


		// применяем полиморфизм
		Gum gum = null;
		switch (chose){
			case 1:
				gum = new BasketballGum();
				break;
			case 2:
				gum = new SoccerGum();
				break;
			default:
				System.err.println("Неправильный выбор");
				System.exit(-1);
		}

		// применяем полиморфизм
		System.out.println("Оборудование: ");
		for (Equipment equipment : gum.getEquipment()){
			System.out.println(equipment);
		}

		// применяем полиморфизм
		System.out.println();
		System.out.println("Начало игры: ");
		gum.play();
		System.out.println("Конец игры");
	}
}
