package gum;

import gum.Equipment;
import gum.Ball;

public abstract class SportEquipment implements Equipment {
	public abstract boolean handle(Ball ball);
}
