package gum;

import java.util.Random;

// применяем наследие
public class SoccerGoals extends SportEquipment{

	@Override
	public void use() {
		System.out.println("Вратарь стоит на воротах и защищает их");
	}

	// применяем полиморфизм
	@Override
	public boolean handle(Ball ball) {
		Random random = new Random();

		if (random.nextInt(101) > 50) {
			System.out.println("Мяч забит");

			return true;
		} else {
			System.out.println("Мяч отбит");

			return false;
		}
	}

	@Override
	public String toString() {
		return "SoccerGoals";
	}
}
