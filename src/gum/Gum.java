package gum;

public abstract class Gum {
	public abstract Equipment[] getEquipment();
	public abstract void play();
}
