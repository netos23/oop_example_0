package gum;

public class BasketballGum extends Gum {
	private BasketballBucket bucket;
	private BasketBall ball;

	public BasketballGum() {
		bucket = new BasketballBucket();
		ball = new BasketBall(12, bucket);
	}

	@Override
	public Equipment[] getEquipment() {
		return new Equipment[]{ball, bucket};
	}

	@Override
	public void play() {
		bucket.use();
		ball.throwBallToBasket();
	}
}
