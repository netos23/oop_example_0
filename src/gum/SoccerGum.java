package gum;

public class SoccerGum extends Gum {
	private SoccerBall ball;
	private SoccerGoals goals;

	public SoccerGum() {
		goals = new SoccerGoals();
		ball = new SoccerBall(21, goals);
	}

	@Override
	public Equipment[] getEquipment() {
		return new Equipment[]{ball, goals};
	}

	@Override
	public void play() {
		goals.use();
		ball.kickBall();
	}


}
