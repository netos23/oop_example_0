package gum;

import gum.Ball;
import gum.SoccerGoals;
// применяем наследование
public class SoccerBall extends Ball {
	private final SoccerGoals soccerGoals;

	public SoccerBall(double radius, SoccerGoals soccerGoals) {
		super(radius, soccerGoals);
		this.soccerGoals = soccerGoals;
	}


	public void kickBall() {
		use();
	}

	// реализуем методы
	@Override
	public int getBallSize() {
		if (radius < 4) {
			return 2;
		} else {
			return 1;
		}
	}

	@Override
	public String getName() {
		return "SocсerBall";
	}

	public SoccerGoals getSoccerGoals() {
		return soccerGoals;
	}
}
