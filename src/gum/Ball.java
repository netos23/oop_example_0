package gum;

public abstract class Ball implements Equipment {
	// применяем инкапсуляцию
	// делаем доступным для потомков
	protected double radius;
	// скрываем реализацию
	private SportEquipment equipment;

	public Ball(double radius, SportEquipment equipment) {
		this.radius = radius;
		this.equipment = equipment;
	}

	public abstract int getBallSize();

	public abstract String getName();


	//имплементируем метод метод
	@Override
	public void use() {
		// применяем полиморфизм
		boolean win = equipment.handle(this);

		if(win){
			System.out.println("Игра выиграна");
		}else{
			System.out.println("Игра проиграна");
		}
	}

	@Override
	public String toString() {
		return getName();
	}
}
